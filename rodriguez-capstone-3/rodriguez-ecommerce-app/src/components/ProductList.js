import { Button } from '@mui/material';
import React, { useState } from 'react'
import {Modal, Form, Container, Row, Col } from "react-bootstrap";
import { Link } from 'react-router-dom';

import Swal from 'sweetalert2';

const ProductList = ({ productProp }) => {

   const { name, _id, quantity, price, description, img, isActive, category} = productProp;

	 const [ name2, setName2 ] = useState(name)
	 const [ img2, setImg2 ] = useState(img)
	 const [ info2, setInfo2 ] = useState(description)
	 const [ price2, setPrice2 ] = useState(price)
	 const [ quantity2, setQuantity2 ] = useState(quantity)
	 const [ category2, setCategory2 ] = useState(category)

	 const orderNo = (_id.replace(/\D/g, "")).slice(0,7);

	//  MODAL CONTROLLERS
   const [showEdit, setShowEdit] = useState(false);
	 const handleClose = () => setShowEdit(false);
   const handleShow = () => setShowEdit(true);

   const editProduct = (e, _id) => {
		e.preventDefault();
		fetch(`${ process.env.REACT_APP_API_URL }/products/`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				id: _id,
				name: name2,
				img: img2,
				description: info2,
				price: price2,
				quantity: quantity2,
				category: category2
			})
		})
		.then(res => res.json())
		.then(data => {
			handleClose();
			if (data === true) {
				Swal.fire({
					title: "Update successful.",
					icon: "success",
					text: "Product successfully updated."
				});		
				window.location.reload();
			} else {
					Swal.fire({
							title: "Something went wrong.",
							icon: "error",
							text: "Please try again."
					});		
			}
		})
	}

	const archiveProduct = () => {
		fetch(`${ process.env.REACT_APP_API_URL }/products/status`,{
			 method: "PUT",
		 headers: {
			 'Content-Type': 'application/json',
				 Authorization: `Bearer ${localStorage.getItem('token')}`
			 },
			 body: JSON.stringify({
					id: _id,
					isActive: isActive,
			 })
		})
		.then(res => res.json())
		.then(data => {
			 if (data === true) {
	
			 } else {
					Swal.fire({
						 title: "Something went wrong.",
						 icon: "error",
					})

			 }
	 
		})
		}
	


   return (
    <>
      <tr key={_id}>
				<td><p className='p-0 m-0' id='btn-link' onClick={handleShow}>{name}</p></td>

				<td>{category}</td>
				<td>{orderNo+name.length}</td>
				<td>{price}</td>
				<td>{quantity}</td>
				<td className='center m-0 p-0 pt-1'>
				{isActive ?
					<Button id='active-btn' onClick={()=>archiveProduct()}>ACTIVE</Button>
				:
        <Button id='inactive-btn' onClick={()=>archiveProduct()}>INACTIVE</Button>
				}
				</td>
		</tr>

      <Modal className="mt-5 pt-5 reg-text px-5" show={showEdit} onHide={handleClose}>

			<Form onSubmit={e => editProduct(e, _id)}>

				<Modal.Header closeButton>
					  <Modal.Title className='lg-text bolder '>Edit Product</Modal.Title>
				</Modal.Header>

				<Modal.Body className='mx-5'>	
					  <Form.Group className='mb-3'>
						<Form.Label>Product Name</Form.Label>
						<Form.Control type="text" className='reg-text' value={name2} onChange={e => setName2(e.target.value)} required/>
					  </Form.Group>

						<Container className='m-0 p-0'>
							<Row className='mb-3'>
								<Col>
										<Form.Group controlId="productName">
										<Form.Label>Price</Form.Label>
										<Form.Control type="text" className='reg-text' value={price2} onChange={e => setPrice2(e.target.value)} required/>
										</Form.Group>
								</Col>
								<Col>
										<Form.Group controlId="productName">
										<Form.Label>Stock</Form.Label>
										<Form.Control type="text" className='reg-text' value={quantity2} onChange={e => setQuantity2(e.target.value)} required/>
										</Form.Group>
								</Col>
							</Row>
						</Container>

					  <Form.Group className='mb-3'>
						<Form.Label>Description</Form.Label>
						<Form.Control type="text" aria-multiline className='reg-text' value={info2} onChange={e => setInfo2(e.target.value)} required/>
					  </Form.Group>

					  <Form.Group className='mb-3'>
						<Form.Label>Category</Form.Label>
						<Form.Control type="text" className='reg-text' value={category2} onChange={e => setCategory2(e.target.value)} required/>
					  </Form.Group>

					  <Form.Group className='mb-3'>
						<Form.Label>Image URL</Form.Label>
						<Form.Control type="text" className='reg-text' value={img2} onChange={e => setImg2(e.target.value)} required/>
					  </Form.Group>

				</Modal.Body>

				<Modal.Footer>
					   <Button id='btn-secondary' onClick={handleClose} >Close</Button>
					   <Button id='btn' type="submit">Submit</Button>
				</Modal.Footer>
			</Form>
		</Modal>
</>  
		



   )
}

export default ProductList