import { Row, Card, Container, Col } from 'react-bootstrap';
import { Link } from "react-router-dom";
import Slider from "react-slick"
import Slide from 'react-reveal/Slide';


import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import 'bootstrap/dist/css/bootstrap.min.css'

export default function ProductCard({productProp}) {

   const {_id, name, price, img}  = productProp
   
   return (
      <Slide bottom>

      <Slider >
           <Card className='m-4 slider' style={{ width: 200, height: 200 }}>
           <Row className="ps-5">
              <Link to={`/products/${_id}`}><img className='hvr-zoom-in' src={img} style={{ width: 200, height: 200, display: "flex", justifyContent: "center", alignItems:"center"}}/></Link>
           </Row>
            {/* <Container className='mt-4 product-card-label flex d-flex justify-content-center align-items-center' >
               <Row>
                  <Col xs={12}>
                        <Link className='link' to={`/products/${_id}`}>{name}</Link>
                  </Col>
                  <Col xs={12}>
                        <Card.Text className='card-price text-primary'>$ {price.toFixed(2)}</Card.Text>
                  </Col>
               </Row>
            </Container>              */}
            </Card>   
            </Slider>      
   </Slide>

   )
}
