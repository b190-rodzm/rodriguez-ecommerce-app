import { React, useState } from 'react'
import { Col, Container, Row } from 'react-bootstrap';
 
export default function App() {
  const [counter, setCounter] = useState(0);
 
  //increase counter
  const increase = () => {
    setCounter(count => count + 1);
  };
 
  //decrease counter
const decrease = () => {
  if (counter > 0) {
    setCounter(count => count - 1);
  }
};
 

 
  return (
    <Container id='counter-box' className='border rounded' style={{width: "120px", height: "50px"}}>
        <Row>
          <Col xs={3} id='counter-box'>
                    <Container fluid className='' id='counter-box' onClick={increase}>+</Container>
          </Col>
          <Col xs={6}>
                    <Container fluid className='' id='counter-box' >{counter + 1}</Container>
          </Col>
          <Col xs={3}>
                    <Container  fluid className='' id='counter-box' onClick={decrease}>-</Container>
          </Col>
        </Row> 
    </Container>
  );
}