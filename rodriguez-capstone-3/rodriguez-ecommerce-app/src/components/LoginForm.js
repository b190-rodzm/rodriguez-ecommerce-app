import React, { useState, useEffect, useContext } from 'react';
import { Modal, Nav, Form, Container} from 'react-bootstrap';

import { Navigate, NavLink, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
  
import { TextField, Button, Typography } from '@mui/material';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';

import Swal from 'sweetalert2';
// import { Link } from 'react-router-dom';

export default function Login() {

  const { user, setUser } = useContext(UserContext);
  const navigate = useNavigate();


  const [checked, setChecked] = React.useState(true);
  const handleChange = (event) => {
         setChecked(event.target.checked);
    };
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);


  const [ email, setEmail ] = useState("");
  const [ password, setPassword ] = useState("");
  const [ isActive, setIsActive ] = useState(false);

	const retrieveUserDetails = (token) => {
		fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data=>{
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
      console.log(data.isAdmin)
      localStorage.setItem('admin', data.isAdmin);



		})
	}

  useEffect(() => {
    if (email !== "" && 
      password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email,password]);

  function loginUser(e) {
    e.preventDefault();

    fetch(`${ process.env.REACT_APP_API_URL }/users/login`,{
			method: 'POST',
			headers:{'Content-type': 'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (typeof data.access !== 'undefined') {

				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title:"Login Successful!",
					icon: "success",
				})
        navigate('/')

        localStorage.setItem('email', email);


			} else{
				Swal.fire({
					title: "Authentication Failed.",
					icon: "error",
					text: "Check your login credentials and try again."
				})
			}
		});
		setEmail('');
		setPassword('');
  }

  return (<>
  {(localStorage.email !== undefined) ?
		<Navigate to='/products' />
		: 
<Container className='half-vh pt-5'>
<h4 className="title-text bolder center my-5">Sign In</h4>
<Form onSubmit ={(e) => loginUser(e)}>
    <Form.Group className="mb-3 px-5 reg-text" controlId="userEmail">
        <TextField label={<Typography variant="label" component="h4">Email *</Typography>} 
        placeholder="johndoe@mail.com"
        type='email'
        id='reg-text'
        fullWidth
        value={email}
        onChange={e=> setEmail(e.target.value)}
        required
        size="large"
        InputLabelProps={{ required: false }}
        ></TextField>
    </Form.Group>

    <Form.Group className="mb-4 px-5" controlId="password">
        <TextField label={<Typography variant="label" component="h4">Password *</Typography>} 
        placeholder="Password"
        type='password'
        id='reg-text'
        fullWidth
        value={password}
        onChange={e=> setPassword(e.target.value)} 
        required
        InputLabelProps={{ required: false }}
        size="large"></TextField>
    </Form.Group>
    <Container>
        <Col className='reg-text center mt-3 mx-5'>
            <NavLink className='reg-text' style={{textDecoration: 'none', color: "black"}} to={'/'}> Forgot password? </NavLink>
        </Col>
        <Row>
            <Col align='center' className='mx-5 mt-3'>
                <Form.Group>
                    {isActive ?
                    <Button fullWidth variant="contained" type="submit" id="btn">Log in</Button>
                    :
                    <Button fullWidth variant="contained" type="submit" id="btn" disabled>Log in</Button>
                    }
                </Form.Group>
            </Col>
        </Row>
    </Container>
</Form>
    <Container>
        <Row>
            <Col className='reg-text center mt-3'>
                <p> New Customer? <NavLink to={'/register'}>Create an Account</NavLink></p>
                <p className='text-center mt-3 mx-5'>
                Sign up today for exclusive member offers, faster checkout, and easier access to your order history!
                </p>
            </Col>                   
        </Row>
    </Container>
</Container>
  }
</>
  );
}