import { useState, useEffect, useContext, Fragment } from 'react';
import { Container, Button, Row, Col, InputGroup, Form } from 'react-bootstrap';
import { useParams, useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2';
import UserContext from '../UserContext';

import Counter from './Counter'

export default function ProductPage(){

	const { user } = useContext(UserContext);
	const navigate = useNavigate();

	const { productId } = useParams();

	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState(0);
	const [ img, setImg ] = useState('');
	const [ qty, setQty ] = useState(1);

	console.log(qty);
 
  //increase counter
  const increase = () => {
    setQty(qty => qty + 1);
  };
 
  //decrease counter
const decrease = () => {
  if (qty > 0) {
    setQty(qty => qty - 1);
  }
};



	useEffect(()=>{

		fetch(`${ process.env.REACT_APP_API_URL }/products/${productId}`)
		.then(res => res.json())
		.then(data=>{
			console.log(data);

			setName(data.name);
			setImg(data.img);
			setDescription(data.description);
			setPrice(data.price);
		})
	},[productId]);

	return(
	<Fragment>
			<Row className='mt-5 pt-5 half-vh'>
				<div className='reg-text text-dark ps-5'><a id="breadcrumb" className='text-muted' href='/'>Home /</a> {name}</div>
						<Col xl={6} className='center'>
								<img src={img} style={{ width: 'auto', height:380 }}></img>
						</Col>
						<Col xl={6} className=''>
								<h1 className='title-text bolder ps-4'>{name}</h1>
								<h1 className='large-text pt-3 ps-4'>$ {price.toFixed(2)}</h1>
								<Container className='reg-text pt-3'>
								<Row> 
										<Container id='counter-box' style={{width: "160px", height: "50px"}}>
												<InputGroup className="mb-3 pt-3 px-0">
        									<Button variant='light' className='counter-btn border' onClick={increase}> + </Button>
        									<Form.Control value={qty}
													className='counter-text text-center counter'
													onChange={e=> setQty(e.target.value)} 
													/>
        									<Button variant='light' className='counter-btn border' onClick={decrease}> - </Button>
      									</InputGroup>
    								</Container>

						
													<div className='col-3'></div>
								</Row>
								</Container>
								<p className='reg-text pt-5 ps-4 bold'>Product Description</p>
									<p className='reg-text ps-4 pe-5'>{description}</p>
									
						</Col>
				</Row>
	</Fragment>

	)
}
