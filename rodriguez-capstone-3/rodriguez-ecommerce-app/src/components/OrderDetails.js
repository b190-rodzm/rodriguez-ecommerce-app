import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { useNavigate, useParams } from 'react-router-dom';
import format from 'date-fns/format'


import OrderItem from "./OrderItem";
import { Button } from '@mui/material';

export default function Orders () {
  const { orderId } = useParams();

  const navigate = useNavigate();

  const [products, setProducts] = useState([]);
  const [date, setDate] = useState([]);
  const [totalAmount, setTotalAmount] = useState([]);

  const orderNo = (orderId.replace(/\D/g, "")).slice(0,7);

  // const orderDate = format(
  //   new Date(date),
  //   'dd MMM yyyy'
  // )

	useEffect(()=> { 
    
		fetch(`${ process.env.REACT_APP_API_URL }/orders/${orderId}`, {
      method: "GET",
      headers: {
				'Content-type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
    })
    .then(res => res.json())
		.then(res=>{
      console.log(res)
      const order = res.products
      setDate(res.purchasedOn)
      setTotalAmount(res.totalAmount)
      setProducts(order.map(res => {
        console.log("hello there")
        return (
          <OrderItem key={res.productId} productProp={res} />
          
        )
      }))
    })

	},[])
  
  return (
      <Container className='half-vh text-center mx-md-5 mt-md-5 pe-md-5 pt-5'>
			<p className='title-text bolder ms-4'>Order Summary</p>
      <Container className='mb-3 menu-text'>
      <Row><Col className='bold col-6 left'> Order No: </Col>{orderNo} </Row>
      {/* <Row><Col className='bold col-6 left'> Order Date: </Col>{orderDate} </Row> */}
      </Container>

      <p className='lg-text bold'>Order Items</p>
        {products}
      <Row className='lg-text pt-5 ms-5 ps-5'><Col className='bold col-6 left'> Total Amount: </Col>$ {totalAmount} </Row>

      <Button id='btn' className='mt-5' onClick={() => navigate(-1)}>Back</Button>


      </Container>
  )
}