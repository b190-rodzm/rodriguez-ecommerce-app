import { Button } from '@mui/material'
import React from 'react'
import { Card, Col, Container, Row } from 'react-bootstrap'

const Cards = () => {
  return (
    <Container fluid className='bg-dark mt-5 py-5'>
      <Row>
        <Col md={6} className='text-center'>
        <Card.Img variant="top" className='bg-info' style={{ width: '500px', height: 'auto'}} src="https://cdn.shopify.com/s/files/1/0496/4332/3542/t/3/assets/pf-87c26b65--tees.jpg?v=1617204127" />
        <Card.Body>
      <Card.Title className='menu-text bolder pt-3 text-light' >AWESOME COOL TSHIRTS</Card.Title>
      <Card.Text>
      </Card.Text>
      <Button id='btn' variant="primary">GRAB YOURS NOW</Button>
    </Card.Body>
        

        </Col>
        <Col md={6} className='text-center'>

        <Card.Img variant="top" className='bg-info' style={{ width: '500px', height: 'auto'}} src="https://cdn.shopify.com/s/files/1/0496/4332/3542/t/3/assets/kidsclothing-1660566956056.jpg?v=1660566957" />
        <Card.Body>
      <Card.Title className='menu-text bolder pt-3 text-light' >FOR GEEK KIDS</Card.Title>
      <Card.Text>
      </Card.Text>
      <Button id='btn' variant="primary">SHOP NOW</Button>
    </Card.Body>

        </Col>
      </Row>
    </Container>
  )
}

export default Cards