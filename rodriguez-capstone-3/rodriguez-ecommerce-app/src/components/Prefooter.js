import { Row, Col, Card, Container } from 'react-bootstrap';


import LocalShippingOutlinedIcon from '@mui/icons-material/LocalShippingOutlined';
import SupportAgentOutlinedIcon from '@mui/icons-material/SupportAgentOutlined';
import SentimentSatisfiedAltOutlinedIcon from '@mui/icons-material/SentimentSatisfiedAltOutlined';
import PaymentIcon from '@mui/icons-material/Payment';

export default function Prefooter(){
	return(
	<Container fluid className='bg-grey text-grey py-3'>
	<Row className='px-5'>
	<Col xs={12} xl={3} md={6} lg={6} className='py-2'>
				<Row>
							<Col xs={2}>
							<div className='center'><LocalShippingOutlinedIcon className='hvr-pop' sx={{fontSize: "4rem"}}/></div>
							</Col>
							<Col>
										<Row>
													<Col xs={12}><h3 id='prefooter' className='right'>FREE SHIPPING & RETURNS</h3>
													</Col><p className='smallText'>Free shipping on all orders</p>
													<Col xs={12}>
													</Col>
										</Row>
							</Col>
				</Row>
	</Col>

	<Col xs={12} xl={3} md={6} lg={6} className='py-2'>
	<Row>
							<Col xs={2}>
							<div className='center'><SupportAgentOutlinedIcon className='hvr-pop' sx={{fontSize: "4rem"}}/></div>
							</Col>
							<Col>
										<Row>
													<Col xs={12}><h3 id='prefooter' className='right'>EXPERT SUPPORT </h3>
													</Col><p className='smallText'>Contact us by chat, mail, phone

</p>
													<Col xs={12}>
													</Col>
										</Row>
							</Col>
				</Row>
	</Col>


	<Col xs={12} xl={3} md={6} lg={6} className='py-2'>
	<Row>
							<Col xs={2}>
							<div className='center'><SentimentSatisfiedAltOutlinedIcon className='hvr-pop' sx={{fontSize: "4rem"}}/></div>
							</Col>
							<Col>
										<Row>
													<Col xs={12}><h3 id='prefooter' className='right'>MONEY BACK GUARANTEE</h3>
													</Col><p className='smallText'>Simply return it within 7 days</p>
													<Col xs={12}>
													</Col>
										</Row>
							</Col>
				</Row>
	</Col>


	<Col xs={12} xl={3} md={6} lg={6} className='py-2'>
	<Row>
							<Col xs={2}>
							<div className='center'><PaymentIcon className='hvr-pop' sx={{fontSize: "4rem"}}/></div>
							</Col>
							<Col>
										<Row>
													<Col xs={12}><h3 id='prefooter' className='right'>100% SECURE PAYMENTS</h3>
													</Col><p className='smallText'>All payment information is encrypted</p>
													<Col xs={12}>
													</Col>
										</Row>
							</Col>
				</Row>
	</Col>
	</Row>
	</Container>

	)
}