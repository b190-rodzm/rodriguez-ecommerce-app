import { Row, Card, Container, Col } from 'react-bootstrap';
import { Link } from "react-router-dom";

export default function ProductCard({productProp}) {

   const {_id, name, price, img}  = productProp
   
   return (
           <Card className='m-1 reg-text center-img pt-4' sx={{ p: 2, border: '1px sol_ lightgrey' }} style={{ width: 222, height: 333 }}>
           <Row className='mt-3'>
              <Link to={`/products/${_id}`}><img className='hvr-zoom-in' src={img} style={{ width: 'auto', height: 180}}/></Link>
           </Row>
            <Container className='mt-4 product-card-label flex d-flex justify-content-center align-items-center' >
               <Row>
                  <Col xs={12}>
                        <Link className='link' to={`/products/${_id}`}>{name}</Link>
                  </Col>
                  <Col xs={12}>
                        <Card.Text className='card-price text-primary'>$ {price.toFixed(2)}</Card.Text>
                  </Col>
               </Row>
            </Container>             
            </Card>         
   )
}
