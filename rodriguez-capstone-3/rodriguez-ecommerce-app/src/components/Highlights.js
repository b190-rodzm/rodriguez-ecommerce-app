import { Row, Col, Card } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
export default function Highlights(){
	return(
		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight bg-info border-0 p-3">
					<Card.Body>
						<Card.Title >
							<h2 className='title-text bolder text-white' style={{textDecoration: 'none'}}>GIFTS</h2>
						</Card.Title>
						<Card.Text></Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight bg-warning border-0 p-3">
					<Card.Body>
						<Card.Title className='title-text bolder text-white'>
							<h2 className='title-text bolder text-white'>GADGETS</h2>
						</Card.Title>
						<Card.Text></Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight bg-secondary border-0 p-3">
					<Card.Body>
						<Card.Title className='title-text bolder text-white'>
							<h2 className='title-text bolder text-white'>MERCH</h2>
						</Card.Title>
						<Card.Text></Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}