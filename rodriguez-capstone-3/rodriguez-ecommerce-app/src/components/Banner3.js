import React from 'react'
import { Container, Row, Col} from 'react-bootstrap'

const Banner3 = () => {
  return (
    <Container>
      <Row className='pt-5 d-sm-none'>
        <Col md={6}>
          <img src="https://cdn.shopify.com/s/files/1/0496/4332/3542/t/3/assets/stragerthings-1648483207352.jpg?v=1648483208" alt="ok" style={{width: 480, height: "auto"}}></img>

        </Col>
        <Col md={6}>
        <img src="https://cdn.shopify.com/s/files/1/0496/4332/3542/t/3/assets/pf-88bb77c6--cableguys.gif?v=1601371399" alt="ok"  style={{width: 480, height: "auto"}}></img>

        </Col>
      </Row>
      <Row className='pt-5'>
        <Col md={6} className='d-none d-sm-block'>
          <img src="https://cdn.shopify.com/s/files/1/0496/4332/3542/t/3/assets/stragerthings-1648483207352.jpg?v=1648483208" alt="ok"  style={{width: 600, height: "auto"}}></img>

        </Col>
        <Col md={6} className='d-none d-sm-block'>
        <img src="https://cdn.shopify.com/s/files/1/0496/4332/3542/t/3/assets/pf-88bb77c6--cableguys.gif?v=1601371399" alt="ok"  style={{width: 600, height: "auto"}}></img>

        </Col>
      </Row>
    </Container>
  )
}

export default Banner3