import React, { useState, useEffect, useContext } from 'react';
import { Modal, Nav, Form, Container} from 'react-bootstrap';

import { TextField, Button, Typography } from '@mui/material';
import { Row } from 'react-bootstrap';
import { Col } from 'react-bootstrap';
import UserContext from '../UserContext';

import Swal from 'sweetalert2';
import { Navigate, useNavigate } from 'react-router-dom';

import { Link } from 'react-router-dom';

export default function Register() {

  const [checked, setChecked] = React.useState(true);
  
  const handleChange = (event) => {
         setChecked(event.target.checked);
    };
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const { user } = useContext(UserContext);
	const navigate = useNavigate();


  const [ email, setEmail ] = useState("");
  const [ password, setPassword ] = useState("");
  const [ password2, setPassword2 ] = useState("");
  const [ firstName, setFirstName ] = useState("");
  const [ lastName, setLastName ] = useState("");

  const [ isActive, setIsActive ] = useState(false);

  console.log(email)
  console.log(password, password2)

  useEffect(()=>{
		if( ( firstName !== '' && lastName !== '' && email !== '' && password !== '' && password2 !== '' ) && ( password === password2 ) ){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [ firstName, lastName, email, password, password2 ]);

  function registerUser(e) {
    e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
		    method: "POST",
		    headers: {
		        'Content-Type': 'application/json'
		    },
		    body: JSON.stringify({
		        email: email
		    })
		})
		.then(res => res.json())
		.then(data => {

		    console.log(data);

		    if(data === true){
		    	Swal.fire({
		    		title: "Email is already in use",
		    		icon: "error",
		    		text: "Please provide another email address."
		    	})
		    }else{
		    	fetch(`${ process.env.REACT_APP_API_URL }/users/register`, {
		    	    method: "POST",
		    	    headers: {
		    	        'Content-Type': 'application/json'
		    	    },
		    	    body: JSON.stringify({
		    	        firstName: firstName,
		    	        lastName: lastName,
		    	        email: email,
		    	        password: password
		    	    })
		    	})
		    	.then(res => res.json())
		    	.then(data => {

		    	    console.log(data);

		    	    if (data === true) {

		    	        // Clear input fields
		    	        setFirstName('');
		    	        setLastName('');
		    	        setEmail('');
		    	        setPassword('');
		    	        setPassword2('');

		    	        Swal.fire({
		    	            title: 'Registration successful',
		    	            icon: 'success',
		    	            text: 'Welcome!'

		    	        });
	    	            navigate('/login')

		    	    } else {

		    	        Swal.fire({
		    	            title: 'Something wrong',
		    	            icon: 'error',
		    	            text: 'Please try again.'   
		    	        });

		    	    }
		    	})
		    }
		})
	}
  

  return (<>
  {(localStorage.email !== undefined) ?
		<Navigate to='/products' />
		: 
<Container className='half-vh pt-5' >
<h4 className="title-text bolder center my-5">Create an Account</h4>


<Form onSubmit ={(e) => registerUser(e)}>


    <Form.Group className="mb-3 px-5 reg-text">
        <TextField label={<Typography variant="label" component="h4">First Name *</Typography>} 
        type='name' color="primary"
        id='reg-text'
        fullWidth
        value={firstName}
        onChange={e=> setFirstName(e.target.value)}
        required
        size="large"
        InputLabelProps={{ required: false }}
        ></TextField>
    </Form.Group>

    <Form.Group className="mb-3 px-5 reg-text">
        <TextField label={<Typography variant="label" component="h4">Last Name *</Typography>} 
        type='name'
        id='reg-text'
        fullWidth
        value={lastName}
        onChange={e=> setLastName(e.target.value)}
        required
        size="large"
        InputLabelProps={{ required: false }}
        ></TextField>
    </Form.Group>

    <Form.Group className="mb-3 px-5 reg-text" controlId="userEmail">
        <TextField label={<Typography variant="label" component="h4">Email *</Typography>} 
        type='email'
        id='reg-text'
        fullWidth
        value={email}
        onChange={e=> setEmail(e.target.value)}
        required
        size="large"
        InputLabelProps={{ required: false }}
        ></TextField>
    </Form.Group>

    <Form.Group className="mb-4 px-5" controlId="password">
        <TextField label={<Typography variant="label" component="h4">Password *</Typography>} 
        type='password'
        id='reg-text'
        fullWidth
        value={password}
        onChange={e=> setPassword(e.target.value)} 
        required
        InputLabelProps={{ required: false }}
        size="large"></TextField>
    </Form.Group>
    <Form.Group className="mb-4 px-5" controlId="password">
        <TextField label={<Typography variant="label" component="h4">Verify Password *</Typography>} 
        type='password'
        id='reg-text'
        fullWidth
        value={password2}
        onChange={e=> setPassword2(e.target.value)} 
        required
        InputLabelProps={{ required: false }}
        size="large"></TextField>
    </Form.Group>
    <Container>
        <Col className='center mt-3 mx-5'>
    <Form.Group required controlId="formBasicCheckbox">
      
      <Form.Check required={true} 
      type="checkbox" 
      align='justified' 
      className='smallText' 
      label="When you agree to the Terms and Conditions you also consent to our use of your personal information to process and operate your account. To see how to control your personal data, please see our privacy policy." />
    </Form.Group>
    </Col>
        <Row>
            <Col align='center' className='mx-5 mt-3'>
                <Form.Group>
                    {isActive ?
                    <Button fullWidth variant="contained" type="submit" id="btn">Create Account</Button>
                    :
                    <Button fullWidth variant="contained" type="submit" id="btn" disabled>Create Account</Button>
                    }
                </Form.Group>
            </Col>
        </Row>
    </Container>
</Form>
    <Container>
        <Row>
            <Col className='reg-text center mt-3'>
                <p>Already have an account? <Link to='/login'> <u>Log in</u></Link> </p>
            </Col>                   
        </Row>
    </Container>
</Container>
  }
</>
  );
}