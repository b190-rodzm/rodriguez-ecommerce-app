import { Row, Col, Form, Container, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}){

	const { title, content, destination, label } = data;
	return(
			<Row>
				<Col className="p-5">
					<h1 className='hero-text bolder p-5 mx-5'>{title}</h1>
					<p className='lg-text mx-5 px-5'>{content}</p>
					<Container className='mx-5 px-5'>
					<Form className="d-flex">
					<Form.Control
              type="search"
              placeholder="Enter your email address"
              className=" reg-text me-2 pe-5 "
              aria-label="Search"
            />
            <Button id='btn'>Enter</Button>
          </Form>
					</Container>

					    
					<Link to={destination} className='ms-5 ps-5 lg-text bolder'>{label}</Link>
				</Col>
			</Row>
		)
}