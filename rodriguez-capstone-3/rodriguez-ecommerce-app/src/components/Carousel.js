import { Carousel } from 'react-bootstrap';

function CarouselUncontrolled() {
  return (<>
    <Carousel fluid className=' bg-dark mt-0'>
          <Carousel.Item>
            <img
              className="slick-slide w-100 d-block mx-auto d-none d-md-block"
              src="https://static.thcdn.com/images/xlarge/webp/widgets/96-en/56/0402-OEV538-jb-z-gaming-page-1920x586-011356.jpg"
              alt="slide"
            />
            <img
              className="slick-slide d-block mx-auto w-100 d-md-none"
              src="https://static.thcdn.com/images/medium/webp/widgets/96-en/23/0402-OEV538-jb-z-gaming-page-750x563-011423.jpg"
              alt="slide-mobile"
            />
           
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="slick-slide w-100 d-block mx-auto d-none d-md-block"
              src="https://static.thcdn.com/images/xlarge/webp/widgets/96-en/22/original-0824-z-STDCRE-38758-z-jb-lotr-1920x586-1-080722.jpg"
              alt="slide"
            />
            <img
              className="slick-slide d-block mx-auto w-100 d-md-none"
              src="https://static.thcdn.com/images/medium/webp/widgets/96-en/36/original-0824-z-STDCRE-38758-z-jb-lotr-750x563-1-080736.jpg"
              alt="slide-mobile"
            />
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="slick-slide w-100 d-block mx-auto d-none d-md-block"
              src="https://static.thcdn.com/images/xlarge/webp/widgets/96-en/35/0726-STDCRE-39214-Z-WR-ZavviMusicPageRebrand-ExclusiveBanners-AttackOnTitan-1920x586-034935.jpg"
              alt="slide"
            />
            <img
              className="slick-slide d-block mx-auto w-100 d-md-none"
              src="https://static.thcdn.com/images/medium/webp/widgets/96-en/43/0726-STDCRE-39214-Z-WR-ZavviMusicPageRebrand-ExclusiveBanners-AttackOnTitan-750x563-034943.jpg"
              alt="slide-mobile"
            />
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="slick-slide w-100 d-block mx-auto d-none d-md-block"
              src="https://static.thcdn.com/images/xlarge/webp/widgets/96-en/49/original-0810_THG0061166_ZH_ZV_STAR_WARS_ANDOR_1920x586-040049.jpg"
              alt="slide"
            />
            <img
              className="slick-slide d-block mx-auto w-100 d-md-none"
              src="https://static.thcdn.com/images/medium/webp/widgets/96-en/05/original-0810_THG0061166_ZH_ZV_STAR_WARS_ANDOR_750x563-040105.jpg"
              alt="slide-mobile"
            />
          </Carousel.Item>


        </Carousel>
    
        </>);
}

export default CarouselUncontrolled;