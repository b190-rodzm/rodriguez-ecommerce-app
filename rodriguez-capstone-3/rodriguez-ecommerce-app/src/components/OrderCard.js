import { Button } from '@mui/material';
import { Card, Col, Container, Row } from 'react-bootstrap';
import { Link } from "react-router-dom";

import format from 'date-fns/format'

export default function OrderCard({orderProp}) {

  const {_id, email, purchasedOn, status, totalAmount, products}  = orderProp

  const orderNo = (_id.replace(/\D/g, "")).slice(0,7)

  const date = format(
    new Date(purchasedOn),
    'dd MMM yyyy'
  )

  return (
           <Card className='m-1 reg-text' sx={{ p: 2, border: '1px solid lightgrey' }} style={{ width: "90vw", height:200 }}>
                <Card.Body className='pb-5 mb-5'>
                <Row className='pt-3 text-center'>
                    <Col xs='6'>
                      <p>Order No:</p>
                        <Card.Text className='bold'>{orderNo}</Card.Text>
                    </Col>
                    <Col xs='6'>
                      <p>Purchase Date:</p>
                      <Card.Text className='bold'>{date}</Card.Text>
                    </Col>
                </Row>
                <Row className='text-center'>
                    <Col xs='6'>
                      <p>Status</p>
                      <Card.Text className='bold' style={{textTransform:"capitalize"}}>{status}</Card.Text>
                    </Col>
                    <Col xs='6'>
                      <p>Order Amount</p>
                      <Card.Text className='bold'>$ {totalAmount.toFixed(2)}</Card.Text>
                    </Col>
                </Row>
                <Row className='pt-4 pb-5 text-center'>
                  <Link id='btn-link-rrd' to={`/orderDetails/${_id}`}><Button className='reg-text' id='btn'> View Order Details</Button></Link>
                </Row>
                </Card.Body>
            </Card>
   
         
  )
}
