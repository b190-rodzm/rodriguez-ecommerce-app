import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import Slider from "react-slick";
import SliderCard from "./SliderCard";


export default function SimpleSlider() {

  const [ products, setProducts ] = useState([]);

  useEffect(()=> { 
      fetch(`${process.env.REACT_APP_API_URL}/products/`)
      .then(res=>res.json())
      .then(data=> {
          setProducts(data.map(product=>{
              return(
                  <SliderCard key={product._id} productProp={product} />	
              );
          }));
      })
  },[])

  var settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    initialSlide: 0,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
            infinite: true,
            dots: true
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            initialSlide: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
  };
  return (
    <Container fluid className='bg-dark px-5 pb-5 pt-5'>
    <p className='title-text bold text-white pt-4 ps-5'>Hot Sellers!</p>
    <Slider {...settings} className='px-5 pb-5 pt-5'>
        {products}
    </Slider>
      </Container>
  );
}