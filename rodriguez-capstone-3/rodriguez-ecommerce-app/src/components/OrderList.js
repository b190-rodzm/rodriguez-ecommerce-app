import React, { useEffect, useState } from 'react'
import format from 'date-fns/format'

import OrderProducts from './OrderProducts'


const OrderList = ({ orderProp }) => {

   const { email, purchasedOn, status, totalAmount, userId, _id} = orderProp;

   const orderNo = (_id.replace(/\D/g, "")).slice(0,7)


   const date = format(
      new Date(purchasedOn),
      'dd MMM yyyy'
    )
   const [products, setProducts] = useState("")

    useEffect(()=> { 
      
      fetch(`${ process.env.REACT_APP_API_URL }/orders/admin/${_id}`)
      .then(res => res.json())
        .then(res=>{
        console.log(res.products)
        const order = res.products
        setProducts(order.map(res => {
          console.log(products)
          return (
            <OrderProducts key={res.productId} productProp={res} />
            
          )
        }))
      })
  
     },[])

  return (
   <tr>
      <td>{email}</td>
      <td>{userId}</td>
      <td>{orderNo}</td>
      <td>{products}</td>
      <td>$ {totalAmount}</td>
      <td>{status}</td>
      <td>{date}</td>
   </tr>
  )
}

export default OrderList