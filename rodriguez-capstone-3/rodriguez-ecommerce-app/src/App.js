import './App.css';

import { UserProvider } from './UserContext';

import { useEffect, useState } from 'react';
import { Container } from 'react-bootstrap'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import AppNavbar from './AppNavbar';

import Home from "./pages/Home";
import Products from "./pages/Products";
import ProductPage from './pages/ProductPage';
import Register from "./pages/Register";
import Account from "./pages/Account";
import AccountInfo from "./components/AccountInfo";
import Cart from "./components/Cart";
import Checkout from "./pages/Checkout";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Error from "./pages/Error";
import Footer from './components/Footer';
import Prefooter from './components/Prefooter';
import OrderDetails from './components/OrderDetails';
import OrderConfirmation from './pages/OrderConfirmation';
import Dashboard from './pages/Dashboard';

function App() {

  const [ user, setUser ] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () =>{
    localStorage.clear();
  }

  useEffect(()=>{
    console.log(user);
  }, [user])

  return (
    <>
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
 
        <Container fluid className='paddingBottom scroll m-0 p-0'>
        <Routes fluid>
          <Route path='/' element={<Home />} />
          <Route path='/dashboard' element={<Dashboard />} />
          <Route path='/products' element={<Products />} />
          <Route path='/products/:productId' element={<ProductPage />} />
          <Route path='/login' element={<Login />} />
          <Route path='/logout' element={<Logout />} />
          <Route path='/register' element={<Register />} />
          <Route path='/account' element={<Account />} />
          <Route path='/accountInfo' element={<AccountInfo />} />
          <Route path='/cart' element={<Cart />} />
          <Route path='/checkout' element={<Checkout />} />
          <Route path='/orderDetails/:orderId' element={<OrderDetails />} />
          <Route path='/orderConfirmation/:orderId' element={<OrderConfirmation />} />
          <Route path='*' element={<Error />} />
        </Routes>
        </Container>
        <Footer />
      </Router>
    </UserProvider>
    </>
  );
}

export default App;