import { Fragment, useEffect, useState } from 'react';
import { Container, Col, Row } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';

export default function Products(){

		const [ products, setProducts ] = useState([]);

		useEffect(()=> { 
				fetch(`${process.env.REACT_APP_API_URL}/products/`)
				.then(res=>res.json())
				.then(data=> {
						setProducts(data.map(product=>{
								return(
										<ProductCard key={product._id} productProp={product} />	
								);
						}));
				})
	},[])

	return(
    <Fragment>
				<h5 className='title-text center bolder pt-5 mt-5'>Products</h5>
						<Container className='title-text center pb-5 mb-5'>
								<Row className='mt-5 justify-content-center'>
									<Col className='product-row justify-content-center'>
									{products}
									</Col> 
								</Row>
						</Container>
    </Fragment>
	)
}