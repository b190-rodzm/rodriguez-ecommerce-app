import { Container } from 'react-bootstrap';
import Banner2 from '../components/Banner2';
import Slide from 'react-reveal/Slide';


export default function Error(){
	const data = {
		title: 'SIGN UP & SAVE',
		content: 'SAVE 15% when you sign up for exclusive Geek Freaks offers and content!',
		destination: '/'
	}

	return(
		<Container fluid className='bg-geek pt-5 pb-5 text-white' >
        <Slide bottom>

		<Banner2 data={data}/>
		</Slide>
		
		</Container>
	)
}