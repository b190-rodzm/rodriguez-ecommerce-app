import React, { useState, useEffect } from 'react';
import { Container, Col, Row, ListGroup, Button } from 'react-bootstrap';
import { TextField, Typography } from '@mui/material';

import Orders from '../components/Orders';
import AccountCard from '../components/AccountCard';

import Swal from 'sweetalert2';

export default function Account(){
	const [active, setActive] = useState([]);

	const [ account, setAccount ] = useState([]);
  const [ firstName, setFirstName ] = useState("");
  const [ lastName, setLastName ] = useState("");
	const [ email, setEmail ] = useState("");
	const [ password, setPassword ] = useState("");
	const [ password1, setPassword1 ] = useState("");
	const [ password2, setPassword2 ] = useState("");

	const [ isActive, setIsActive ] = useState("false");


	const token = localStorage.token

	useEffect(()=> { 
		fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
				headers:{ Authorization: `Bearer ${token}`}
			})
			.then(res => res.json())
			.then(user=>{
				setActive("Orders")
					setAccount(user)
					setFirstName(user.firstName)
					setLastName(user.lastName)
					setEmail(user.email)
					return(
							<AccountCard key={user._id} accountProp={user} />
					)
			})
	},[])

	const getProducts = () => {
	fetch(`${ process.env.REACT_APP_API_URL }/products/`)
    .then(res => res.json()) 
    .then(data => {
      return data;
    })
	}

	const updateProfile = () => {

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
					'Content-Type': 'application/json'
			},
			body: JSON.stringify({
					email: email
			})
	})
	.then(res => res.json())
	.then(data => {

			console.log(data);

			if(data === true){
				Swal.fire({
					title: "Email is already in use",
					icon: "error",
					text: "Please provide another email address."
				})
			}else{
				fetch(`${ process.env.REACT_APP_API_URL }/users/details/`, {
					method: 'PUT',
					headers: {
						'Content-type': 'application/json',
						Authorization: `Bearer ${localStorage.getItem('token')}`
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email
					})
				})
					.then(res=> res.json())
					.then(data => {
						if (data === true) {
			
							Swal.fire({
									title: 'Account successfully updated',
									icon: 'success',
							});
			
					} else {
			
							Swal.fire({
									title: 'Something went wrong.',
									icon: 'error',
									text: 'Please try again.'   
							});
					}
					})
			}
		})

					
	}

	useEffect(()=>{
		if( ( password !== '' && password1 !== '' && password2 !== '') && ( password1 === password2 ) ){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [ password, password, password2 ]);

	const changePassword = () => {
	fetch(`${ process.env.REACT_APP_API_URL }/users/details/`, {
		method: 'POST',
		headers: {
			'Content-type': 'application/json',
			Authorization: `Bearer ${localStorage.getItem('token')}`
		},
		body: JSON.stringify({
			oldPassword: password,
			newPassword: password1,
			password2: password2
		})
	})
		.then(res=> res.json())
		.then(data => {
			console.log(data)

			if (data === true) {

				setPassword('');
				setPassword1('');
				setPassword2('');

				Swal.fire({
						title: 'Password successfully changed',
						icon: 'success',
				});

		} else {

			setPassword('');
			setPassword1('');
			setPassword2('');

				Swal.fire({
						title: 'Incorrect password!',
						icon: 'error',
						text: 'Please try again.'   
				});
		}

		})
	}


	return(<>
		<Container>
		<Row className="mt-3 p-5 mx-md-4">

		<Col md={4} className="mt-5 pt-5">
			<h1 className='title-text bolder'>My Account</h1>
			{/* <Nav.Link className='menu-text pt-3' to={"#"}>Shopping Cart</Nav.Link> */}

			<ListGroup className='menu-text pt-5' variant="flush" defaultActiveKey="#link1">
      <ListGroup.Item className='menu-text py-4 account-menu' action variant='light' onClick={()=> {getProducts(); setActive("Orders")}}>
			Orders
      </ListGroup.Item>
      <ListGroup.Item className='menu-text py-4' action variant='light' onClick={()=> {getProducts(); setActive("AccountInfo")}}>
			Account Details
      </ListGroup.Item>

      <ListGroup.Item  className='menu-text py-4' action variant='light' >
      Address Book
      </ListGroup.Item>
      <ListGroup.Item  className='menu-text py-4' action variant='light' >
      Payment Methods
      </ListGroup.Item>
      <ListGroup.Item  className='menu-text py-4 mb-4' action variant='light' href="/logout">
     Logout
      </ListGroup.Item>

    </ListGroup>
		</Col>
		<Col md={8} className="mt-md-5 pt-md-5">

		{active === "Orders" &&

		<Orders />
		}
		{active === "AccountInfo" &&
		<>

				<Container className='reg-text pt-md-3 mx-md-5 pe-md-5'>
				<p className="menu-text bold pb-3 ps-3">Account Details</p>
										<TextField
											label={<Typography variant="label" component="h4">First Name</Typography>} 
											value={firstName}
											onChange={e=> setFirstName(e.target.value)}
											variant="outlined"
											id='reg-text'
											fullWidth
											className='pb-4 pe-xl-5 me-xl-5'
											/>
										<TextField
											label={<Typography variant="label" component="h4">Last Name</Typography>} 
											variant="outlined" 
											className='reg-text pb-5 mb-3 pe-xl-5 me-xl-5' 
											id='reg-text'
											value={lastName}
											onChange={e=> setLastName(e.target.value)}
											fullWidth
											/>

				</Container>


				<Container className='reg-text  mx-md-5 pe-md-5'>
						<TextField
							label={<Typography variant="label" component="h4">Email</Typography>} 
							value={email}
							onChange={e=> setEmail(e.target.value)}
							variant="outlined"
							id='reg-text'
							fullWidth
							className='pb-4 pe-xl-5 me-xl-5'
							/>
		<Button id='btn' onClick={()=>updateProfile()}>save changes</Button>
				</Container>

				<Container className='reg-text pt-5  mx-md-5 pe-md-5'>
						<TextField
							label={<Typography variant="label" component="h4">Old Password</Typography>} 
							value={password}
							type='password'
							placeholder='******'
							onChange={e=> setPassword(e.target.value)}
							variant="outlined"
							id='reg-text'
							fullWidth
							className='pb-3 mb-3 pe-xl-5 me-xl-5'
							/>
				</Container>
		
				<Container className='reg-text  mx-md-5 pe-md-5'>

										<TextField
											label={<Typography variant="label" component="h4">New Password</Typography>} 
											value={password1}
											type='password'
											onChange={e=> setPassword1(e.target.value)}
											variant="outlined"
											id='reg-text'
											fullWidth
											className='pb-3 pe-xl-5 me-xl-5'
											/>

										<TextField
											label={<Typography variant="label" component="h4">Confirm New Password</Typography>} 
											variant="outlined" 
											className='reg-text pb-5 pe-xl-5 me-xl-5' 
											id='reg-text'
											value={password2}
											type='password'
											fullWidth
											onChange={e=> setPassword2(e.target.value)}
											/>

		{isActive ?
                    <Button fullWidth variant="contained" type="submit" id="btn" onClick={()=>changePassword()}>Update Password</Button>
                    :
                    <Button fullWidth variant="contained" type="submit" id="btn" disabled>Update Password</Button>
                    }
				</Container>


		</>
		}
		</Col>
		<Col>
		</Col>

	</Row>
		</Container>
	</>
	)
	}
