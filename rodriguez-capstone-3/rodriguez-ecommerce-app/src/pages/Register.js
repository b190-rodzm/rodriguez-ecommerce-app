import "../App.css";

import {Container, Row, Col, Button} from "react-bootstrap";

import RegisterForm from '../components/RegisterForm'
import { Link } from "react-router-dom";
import Footer from "../components/Footer";

export default function App() {

  return (<>
      <Container className='p-0'>
          <Row>

              <Col lg={3}>
              </Col>

              <Col lg={6}>
              <RegisterForm />
              </Col>

              <Col lg={3}>
              </Col>

          </Row>
      </Container>
  </>
  );
}
