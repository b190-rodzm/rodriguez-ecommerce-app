import React, { useEffect, useState } from 'react'
import { Container, Table, Row, Col, Button, Form } from "react-bootstrap";
import UserList from "../components/UserList";
import ProductList from "../components/ProductList";
import OrderList from "../components/OrderList";
import { TextField, Typography } from '@mui/material';
import Swal from 'sweetalert2';
import { Navigate } from 'react-router-dom';


const Dashboard = () => {

   const [users, setUsers] = useState([]);
   const [active, setActive] = useState([]);
   const [products, setProducts] = useState([]);
   const [orders, setOrders] = useState([]);

   const [productName, setProductName] = useState([]);
   const [img, setImg] = useState([]);
   const [description, setDescription] = useState([]);
   const [price, setPrice] = useState([]);
   const [quantity, setQuantity] = useState([]);
   const [category, setCategory] = useState([]);

   const adminKey = () => {
      if (localStorage.admin === "true") {
        return true;
      } else {
        return false;
      }
    }
    const adminData = adminKey()

   useEffect(() => {
      getProducts()
      getUsers()
    },[products, users])
   
   const getUsers = () => {
      fetch(`${ process.env.REACT_APP_API_URL }/users/`)
      .then(res => res.json())
      .then(data => {
         setUsers(data.map(user => {
            return (
               <UserList key={user._id} userProp={user} />
            )
         }))
      })
   }
   
      const getProducts = () => {
         fetch(`${ process.env.REACT_APP_API_URL }/products/all`)
         .then(res => res.json())
         .then(data => {
            setProducts(data.map(product => {
               return (
                  <>
                  <ProductList key={product._id} productProp={product} />
                  </>
               )
            }))
         })
      }

   const getOrders = () => {
         fetch(`${ process.env.REACT_APP_API_URL }/users/orders`,{
            method: "POST",
            headers: {
               'Content-Type': 'application/json',
               Authorization: `Bearer ${localStorage.getItem('token')}`
               }
         })
         .then(res => res.json())
         .then(data => {
            console.log(data);
            setOrders(data.map(order => {
               return (
                  <OrderList key={order._id} orderProp={order}/>
               )
            }))
         })
   }

      const addProduct = () => {
         fetch(`${ process.env.REACT_APP_API_URL }/products/`,{
            method: "POST",
		      headers: {
		        'Content-Type': 'application/json',
              Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
               name: productName,
               price: price,
               img: img,
               description: description,
               quantity: quantity,
               category: category
            })
         })
         .then(res => res.json())
         .then(data => {
            if (data === true) {
               setProductName("")
               Swal.fire({
                  title:"Product added.",
                  icon: "success",
               })
               getProducts()
            } else {
               Swal.fire({
                  title: "Something went wrong.",
                  icon: "error",

               })

            }
        
         })
         }




        

   return (
<>
{( adminData === false ) ?
   <Navigate to={'/products'} />
:

<Container fluid className="mt-5 pt-5 px-5 half-vh">
   <Row className='pb-5 '>
      <Col xl={2}>
            <p className='title-text bolder pb-5'>Dashboard</p>
            <Container className="p-0 pb-5">
               <Row>
                  <Col xs={12}>
                  <Button variant='light' className="col-12 right menu-text text-dark py-3" onClick={()=> {getProducts(); setActive("ProductsData")} }>Products</Button>
                  </Col>
                  <Col xs={12}>
                  <Button variant='light' className="col-12 right menu-text text-dark py-3"  onClick={()=> {getOrders(); setActive("OrdersData")} }>Orders</Button>
                  </Col>
                  <Col xs={12}>
                  <Button variant='light' className="col-12 right menu-text text-dark py-3"  onClick={()=> {getUsers(); setActive("UsersData")} }>Customers</Button>
                  </Col>

               </Row>
            </Container>
      </Col>

      <Col xl={10} className='bg-info' style={{overflowY: "scroll"}}>
       <Row>
       {active === "ProductsData" &&
       <Col  className='col-12 mt-3'>
       <Button id='btn' className='col-12 menu-text py-1 my-2' onClick={()=> {getUsers(); setActive("AddProduct")} }>Add Product</Button>
       </Col>
       }
       {active === "UsersData" &&
       <Col  className='col-12 mt-4'><div className='col-12 btn menu-text py-3 my-3'></div>
       </Col>
       }
       {active === "OrdersData" &&
       <Col  className='col-12 mt-4'><div className='col-12 btn menu-text py-3 my-3'></div>
       </Col>
       }
       <Col className='col-12 mt-5'>

      {active === "UsersData" &&
<Table className='reg-text' striped bordered hover size="sm">
<thead>
   <tr>
      <th className='py-3'>First Name</th>
      <th className='py-3'>Last Name</th>
      <th className='py-3'>Email</th>
      <th className='py-3'>Customer ID</th>
      <th className='py-3'>Admin Status</th>
   </tr>
</thead>
<tbody>
   {users }
      
</tbody>
</Table>
}
{active === "ProductsData" &&
<>
 <Table className='reg-text mt-3' striped bordered hover size="sm">
    <thead  >
       <tr>
          <th className='py-3'>Name</th>
          <th className='py-3'>Category</th>
          <th className='py-3'>Product No.</th>
          <th className='py-3'>Price</th>
          <th className='py-3'>Stock</th>
          <th className='py-3'>Status</th>
       </tr>
    </thead>
    <tbody>
       {products}                
    </tbody>
 </Table>
 </>
    }
    {active === "OrdersData" &&
 <Table className='reg-text' scroll striped bordered hover size="sm">
    <thead>
       <tr>
          <th className='py-3'>Customer Email</th>
          <th className='py-3'>Customer ID</th>
          <th className='py-3'>Order No.</th>
          <th className='py-3'>Products Ordered</th>
          <th className='py-3'>Total Amount</th>
          <th className='py-3'>Order Status</th>
          <th className='py-3'>Order Date</th>
       </tr>
    </thead>
    <tbody>
       {orders}                
    </tbody>
 </Table>
    }
    {active === "AddProduct" &&
 <Form onSubmit={()=>{addProduct()}}>
<Form.Group className="mb-3 px-5 reg-text">
    <TextField label={<Typography variant="label" component="h4">Product Name *</Typography>} 
    type='name' color="primary"
    id='reg-text'
    className="mb-3"
    fullWidth
    value={productName}
    onChange={e=> setProductName(e.target.value)}
    required
    size="large"
    InputLabelProps={{ required: false }}
    ></TextField>

    <TextField label={<Typography variant="label" component="h4">Category *</Typography>} 
    type='name' color="primary"
    id='reg-text'
    className="mb-3"
    fullWidth
    value={category}
    onChange={e=> setCategory(e.target.value)}
    required
    size="large"
    InputLabelProps={{ required: false }}
    ></TextField>
<Container className='p-0'>
   <Row>
      <Col>
         <TextField label={<Typography variant="label" component="h4">Price *</Typography>} 
         type='number' color="primary"
         id='reg-text'
         className="mb-3"
         fullWidth
         value={price}
         onChange={e=> setPrice(e.target.value)}
         required
         size="large"
         InputLabelProps={{ required: false }}
         ></TextField>
      </Col>

      <Col>
         <TextField label={<Typography variant="label" component="h4">Stock *</Typography>} 
         type='number' color="primary"
         id='reg-text'
         className="mb-3"
         fullWidth
         value={quantity}
         onChange={e=> setQuantity(e.target.value)}
         required
         size="large"
         InputLabelProps={{ required: false }}
         ></TextField>
      </Col>
   </Row>
</Container>


    <TextField label={<Typography variant="label" component="h4">Description *</Typography>} 
    type='name' color="primary"
    id='reg-text'
    className="mb-3"
    fullWidth
    value={description}
    onChange={e=> setDescription(e.target.value)}
    required
    multiline
    size="large"
    InputLabelProps={{ required: false }}
    ></TextField>

    <TextField label={<Typography variant="label" component="h4">Image URL *</Typography>} 
    type='name' color="primary"
    id='reg-text'
    className="mb-3"
    fullWidth
    value={img}
    onChange={e=> setImg(e.target.value)}
    required
    size="large"
    InputLabelProps={{ required: false }}
    ></TextField>
    <Button id='btn' onClick={()=> {addProduct(); setActive("ProductsData")} }> Add Product</Button>
</Form.Group>


</Form>
    }

</Col>
</Row>
</Col>
</Row>
</Container>
}
</>
)
}
export default Dashboard

