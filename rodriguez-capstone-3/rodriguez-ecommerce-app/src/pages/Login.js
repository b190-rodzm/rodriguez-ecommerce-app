import "../App.css";
import {Container, Row, Col} from "react-bootstrap";
import LoginForm from '../components/LoginForm'

export default function App() {

  return (<>
      <Container>
          <Row>
              <Col lg={3}>
              </Col>
              <Col lg={6}>
              <LoginForm />
              </Col>
              <Col lg={3}>
              </Col>
          </Row>
      </Container>
  </>
  );
}
