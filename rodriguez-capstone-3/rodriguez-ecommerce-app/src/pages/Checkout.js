import { Button, TextField, Typography } from '@mui/material';
import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Form} from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2';

import CheckoutItem from "../components/CheckoutItem";

export default function OrderView(params) {

	const [products, setProducts] = useState([]);
  const [products2, setProducts2] = useState([]);
  const [orderId, setOrderId] = useState([]);
  const [totalAmount, setTotalAmount] = useState([]);


  const [email, setEmail] = useState([]);
  const [firstName, setFirstName] = useState([]);
  const [lastName, setLastName] = useState([]);

  

  const [active, setActive] = useState([]);
  const navigate = useNavigate();


  const token = localStorage.token;
  const finalAmount =  Math.round((totalAmount + Number.EPSILON) * 100) / 100;

  const checkout = () => {
    fetch(`${ process.env.REACT_APP_API_URL }/users/checkOut`, {
      method: "POST",
      headers: {
				'Content-type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
    })
    .then(res=> res.json())
    .then(data => {
			console.log(data);
			if (data[1] === true) {
        Swal.fire({
					title: "Order placed, thanks!",
					icon: "success",
				});
        console.log(data[0])
        navigate(`/orderConfirmation/${data[0]}`)
                
      } else {
        Swal.fire({
          title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
      }
    })
  }

	useEffect(()=>{
    fetch(`${ process.env.REACT_APP_API_URL }/cart`, {
      method: 'GET',
      headers: {
        'Content-type': 'application/json',
				Authorization: `Bearer ${token}`
      }
    })
    .then(res => res.json()) 
    .then(res => {
      setProducts2(res.map(info => {
        const data = info.products
        console.log(data)
        const totalAmount = data.map(item => item.subTotal).reduce((prev, curr) => prev + curr, 0);
        setTotalAmount(totalAmount);
        setActive("CheckoutInfo")
        console.log(totalAmount);
        return(
          setProducts(data.map(res => {
          return(
          <CheckoutItem key={res.productId} productProp={res} />
          )
        }))
        );
      }));
    })
  },[])

  useEffect(()=>{

  fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
    headers:{
      Authorization: `Bearer ${token}`
    }
  })
  .then(res => res.json())
  .then(data=>{
    setFirstName(data.firstName)
    setLastName(data.lastName)
    setEmail(data.email)

    console.log(data)



  })
},[])











	return (<>

<Container className='px-5'>
<div className='pt-5 px-md-5' style={{marginBottom: '30rem'}}>
<Row className='col-12 mt-5 pt-5 half-vh'>

      {active === "CheckoutInfo" && 
      <Col xl={7}>
        <p className='title-text bolder'>Checkout Information</p>
        <Container className='pt-5'>
          <Row>
            <Col md={6}>
                <TextField 
                label={<Typography variant="label" component="h4">First Name</Typography>} 
                value={firstName}
                onChange={e=>setFirstName(e.target.value)}
                variant="outlined" id='reg-text' fullWidth
                className='col-12 pb-4 me-xl-5 '/>
            </Col>

            <Col md={6}>

        <TextField 
        label={<Typography variant="label" component="h4">Last Name</Typography>} 
        value={lastName}
        onChange={e=> setLastName(e.target.value)}
        variant="outlined" id='reg-text' fullWidth
        className='col-12 pb-4 me-xl-5'/>
            </Col>

<Col md={12}>

        <TextField 
        label={<Typography variant="label" component="h4">Address Line 1</Typography>} 
        value=""
        onChange={e=> (e.target.value)}
        variant="outlined" id='reg-text' fullWidth
        className='col-12 pb-4 me-xl-5'/>
            </Col>

<Col md={12}>

        <TextField 
        label={<Typography variant="label" component="h4">Address Line 2 (Optional)</Typography>} 
        value=""
        onChange={e=> (e.target.value)}
        variant="outlined" id='reg-text' fullWidth
        className='col-12 pb-4 me-xl-5'/>
            </Col>

<Col md={8}>

        <TextField 
        label={<Typography variant="label" component="h4">Town / City</Typography>} 
        value=""
        onChange={e=> (e.target.value)}
        variant="outlined" id='reg-text' fullWidth
        className='col-6 pb-4 me-xl-5'/>
            </Col>

<Col md={4}>

        <TextField 
        label={<Typography variant="label" component="h4">ZIP / PostCode</Typography>} 
        value=""
        onChange={e=> (e.target.value)}
        variant="outlined" id='reg-text' fullWidth
        className='col-6 pb-4 me-xl-5'/>
            </Col>

<Col md={12}>

        <TextField 
        label={<Typography variant="label" component="h4">Mobile number</Typography>} 
        value=""
        onChange={e=> (e.target.value)}
        variant="outlined" id='reg-text' fullWidth
        className='pb-4 me-xl-5'/>
            </Col>

            <Col className='left'>
        <Button id='btn' onClick={()=> {setActive("DeliveryInfo")} }>Continue to Shipping</Button>

            </Col>

          </Row>
        </Container>
      </Col>
    }

    {active === "DeliveryInfo" &&
      <Col>
        <p className='title-text bolder'>Delivery Information</p>
        <Container className='pt-5'>
          <Row>
            <Col md={12}>
                <TextField 
                label={<Typography variant="label" component="h4">Email</Typography>} 
                value={email}
                onChange={e=> setEmail(e.target.value)}
                variant="outlined" id='reg-text' fullWidth
                className='col-12 pb-4 me-xl-5 '/>
            </Col>
            <Form>

          <Form.Check 
          className="reg-text mb-3 pt-5"
            type="radio"
            id="radio"
            label="Free Shipping"
          />
          <Form.Check 
          className="reg-text mb-3 pb-5"
            type="radio"
            id="radio"
            label="Express Shipping"
          />
           </Form>

          </Row>

            <Col className='left d-flex justify-content-between'>
            <Button id='offcanvas-center-btn' onClick={()=> {setActive("CheckoutInfo")} }>Back to Checkout Info</Button>
        <Button id='btn' onClick={()=> {setActive("PaymentInfo")} }>Continue to Payment</Button>

            </Col>

        </Container>
      </Col>
    }

    {active === "PaymentInfo" && 
      <Col>
        <p className='title-text bolder'> Payment</p>
        <p className='reg-text' >All transactions are secured and encrypted for your protection.</p>
        <Container className='pt-5'>
          <Row>
            <Col md={12}>
                <TextField 
                label={<Typography variant="label" component="h4">Cardholder Name</Typography>} 
                value=""
                onChange={e=> (e.target.value)}
                variant="outlined" id='reg-text' fullWidth
                className='col-12 pb-4 me-xl-5 '/>
            </Col>

            <Col md={12}>

        <TextField 
        label={<Typography variant="label" component="h4">Card Number</Typography>} 
        type="Password"
        value=""
        onChange={e=> (e.target.value)}
        variant="outlined" id='reg-text' fullWidth
        className='col-12 pb-4 me-xl-5'/>
            </Col>

<Col md={6}>

        <TextField 
        label={<Typography variant="label" component="h4">Expiration Date (MM/YY)</Typography>} 
        value=""
        onChange={e=> (e.target.value)}
        variant="outlined" id='reg-text' fullWidth
        className='col-12 pb-4 me-xl-5'/>
            </Col>

<Col md={6}>

        <TextField 
        label={<Typography variant="label" component="h4">CVV (3 Digits)</Typography>} 
        value=""
        onChange={e=> (e.target.value)}
        variant="outlined" id='reg-text' fullWidth
        className='col-6 pb-4 me-xl-5'/>
            </Col>

            <Col className='left d-flex justify-content-between'>
            <Button id='offcanvas-center-btn' onClick={()=> {setActive("DeliveryInfo")} }>Back to Shipping</Button>

        <Button id='btn' onClick={()=>checkout()}>Place Order</Button>

            </Col>

          </Row>
        </Container>
      </Col>
    }

    {/* {ORDER SUMMARY} */}
      <Col xl={5}>
		      <Row className='m-0'>
			      <Col className='me-5 ps-5 pt-3 mb-3 border border-solid'>
            <p className='menu-text bold'>Order Summary</p>
		        </Col> 
			      <Col className='me-5 ps-5 border border-solid'>
			         {products}
		        </Col> 

			      <Col className='container me-5 ps-5 pt-3 mt-3 border border-solid'>
            <Container>
            <Row>
            <Col md={4}>
			         <p className='ps-5 title-text bolder'>Total</p>
            </Col>
            <Col md={3}>
            <p> including VAT</p>
            </Col>
            <Col md={5} className=''>
            <p className='ps-5 title-text bolder'> $ {finalAmount}</p>
            </Col>
            </Row>
            </Container>
		        </Col> 
		      </Row>
      </Col>

</Row>
</div>
</Container>
</>
	)
};
