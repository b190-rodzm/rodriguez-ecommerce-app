import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import Slide from 'react-reveal/Slide';
import Zoom from 'react-reveal/Zoom';
import Fade from 'react-reveal/Fade';


import Cards from '../components/Cards';


import Carousel from '../components/Carousel';
import Highlights from '../components/Highlights';
import Slider from '../components/Slider';
import Hero from './Hero'
import Banner3 from '../components/Banner3'

export default function Home(){

	return(
		
		<Fragment>
		    <Container fluid className='bg-danger mt-3 pb-1 text-center text-light menu-text bolder'>
       		 <p className='mt-4 pt-5'>Only Today! UP TO 50% OFF! SHOP NOW!</p>
    		</Container>


			<Carousel />


			<Highlights />

	

			<Slider/>

		
			<Fade bottom>


			<Hero />
			</Fade>

			<Fade bottom>
			<Fade bottom>
				{/* <Banner3 /> */}
			</Fade>
				{/* <Cards></Cards> */}
			</Fade>
		</Fragment>
	)
}